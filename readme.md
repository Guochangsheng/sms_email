## SMS and Email Service
1. 项目使用`nodejs`，demo 使用`express`框架

### SMS使用阿里云SDK(nodejs)
1. 安装核心库 require('@alicloud/sms-sdk');
2. 发送短信需要 创建（短信签名、短信模版）
3. 生成`accessKeyId`和`sercretAccessKey`

### 邮件服务
1. 安装库`nodemailer`和`nodemailer-smtp-transport`
2. 使用QQ邮箱服务，可以生成一个非登陆密码的密码

## 使用说明
运行服务 `node server/main.js`

发送邮件 `http://localhost:8080/html/email.html`

发送短信 `http://localhost:8080/html/sms.html`