const express=require('express');
const path=require('path');
var bodyParser = require('body-parser');

//邮件
const nodemailer=require('nodemailer');
const smtpTransport=require('nodemailer-smtp-transport');

//短信
const SMSClient = require('@alicloud/sms-sdk');

//配置信息
const CFG=require('./config.json');

const { text, json } = require('express');
const { config } = require('process');
const { join } = require('path');

var app=express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname,'../','statics')));

app.listen(8080,function(){
    console.log('server run at port 8080');

    //初始化邮件服务
    global.email=new Email();
    //初始化短信服务
    global.sms=new SMS();
});

app.get('/',function(req,res){
    res.send('Hello world, current time is,'+(+new Date()));
});

//发送邮件接口
app.post('/api/sendemail',function(req,res){
    var to=req.body.to,
        title=req.body.title,
        body=req.body.body,
        ishtml=(req.body.ishtml*1)==2;
    var toes=to.split(';');
    var result=[];
    var cb=function(){
        if(result.length==toes.length)
            res.send('This result<br>'+result.join('<br>'));
    };
    for(var i=0,j=toes.length;i<j;i++){
        var _t=toes[i];
        if(_t!=''){
            email.Send(_t,title,body,ishtml,function(){
                result.push('send email to '+toes[result.length]+' succces!');
                cb();
            },function(err){
                result.push('send email to '+toes[result.length]+' failed by '+JSON.stringify(err));
                cb();
            });
        }
        
    }
});

//发送短信接口
app.post('/api/sendsms',function(req,res){
    var to=req.body.to,
        name=req.body.name;
    var toes=to.split(';'),
        names=name.split(';');
    var result=[];
    var cb=function(){
        if(result.length==toes.length)
            res.send('This result<br>'+result.join('<br>'));
    };
    for(var i=0,j=toes.length;i<j;i++){
        var _t=toes[i],_n=names[i];
        if(_t!=''&&_n!=''){
            sms.Send(_t,JSON.stringify({'code':_n}),function(){
                result.push('send sms to '+_t+'['+_n+']'+' succces!<br>');
                cb();
            },function(err){
                result.push('send sms to '+_t+'['+_n+']'+' failed by '+JSON.stringify(err)+'<br>');
                cb();
            });
        }
    }
});

//发送短信接口 模版1
app.post('/api/sendsms1',function(req,res){
    var to=req.body.to,
        lockName=req.body.lockName,
        payload=req.body.payload,
        startDateTime=req.body.startDateTime;
    if(lockName!=''&&payload!=''){
        sms.SendTemplate(to,JSON.stringify({'lockName':lockName,'payload':payload,'startDateTime':startDateTime}),'SMS_202818599',function(){
            res.send('send sms to '+to+' succces!<br>');
        },function(err){
            res.send('send sms to '+to+' failed by '+JSON.stringify(err)+'<br>');
        });
    }else{

    }
});
//发送短信接口 模版2
app.post('/api/sendsms2',function(req,res){
    var to=req.body.to,
        lockName=req.body.lockName,
        payload=req.body.payload,
        startDateTime=req.body.startDateTime,
        endDateTime=req.body.endDateTime;
    if(lockName!=''&&payload!=''){
        sms.SendTemplate(to,JSON.stringify({'lockName':lockName,'payload':payload,'startDateTime':startDateTime,'endDateTime':endDateTime}),'SMS_202823794',function(){
            res.send('send sms to '+to+' succces!<br>');
        },function(err){
            res.send('send sms to '+to+' failed by '+JSON.stringify(err)+'<br>');
        });
    }
});
//发送短信接口 模版3
app.post('/api/sendsms3',function(req,res){
    var to=req.body.to,
        lockName=req.body.lockName,
        payload=req.body.payload,
        startDateTime=req.body.startDateTime;
    if(lockName!=''&&payload!=''){
        sms.SendTemplate(to,JSON.stringify({'lockName':lockName,'payload':payload,'startDateTime':startDateTime}),'SMS_202823798',function(){
            res.send('send sms to '+to+' succces!<br>');
        },function(err){
            res.send('send sms to '+to+' failed by '+JSON.stringify(err)+'<br>');
        });
    }
});
//发送短信接口 模版4
app.post('/api/sendsms4',function(req,res){
    var to=req.body.to,
        payload=req.body.payload,
        duration=req.body.duration;
    if(payload!=''){
        sms.SendTemplate(to,JSON.stringify({'payload':payload,'duration':duration}),'SMS_203195440',function(){
            res.send('send sms to '+to+' succces!<br>');
        },function(err){
            res.send('send sms to '+to+' failed by '+JSON.stringify(err)+'<br>');
        });
    }
});

function Email(){
    this.from=CFG.account;
    this.service=nodemailer.createTransport(smtpTransport({
        host:CFG.smtp,
        secure:true,
        secureConnection:true,
        port:CFG.port,
        auth:{
            user:CFG.account,
            pass:CFG.password
        }
    }));
    return this;
}

Email.prototype.Send=function(to,subject,body,isHtml,successCallback,failedCallback){
    var options={
        from:this.from,
        to:to,
        subject:subject
    }
    if(isHtml) options.html=body;
    else options.text=body;
    try{
        this.service.sendMail(options,function(err,info){
            if(err){
                console.log('send email error ',err);
                failedCallback&&failedCallback(err);
                return;
            }
            console.log('send email success');
            successCallback&&successCallback();
        });
    }catch(err){
        console.log('send email exception ',err);
        failedCallback&&failedCallback(err);
    }
}

function SMS(){
    var accessKeyId=CFG.accessKeyID,
        secretAccessKey=CFG.accessKey;
    this.SignName=CFG.signName;
    this.TestTemplateCode=CFG.testTemplateCode;
    this.client=new SMSClient({accessKeyId,secretAccessKey});
    return this;
}
//发送短信
SMS.prototype.Send=function(toPhoneNumbers,params,successCallback,failedCallback){
    this.client.sendSMS({
        PhoneNumbers:toPhoneNumbers,
        SignName:this.SignName,
        TemplateCode:this.TestTemplateCode,
        TemplateParam:params
    }).then(function(res){
        console.log('send sms',res);
        if(res.Message=='OK'){
            console.log('send sms success',toPhoneNumbers,params,res);
            successCallback&&successCallback();
        }
    },function(err){
        console.log('send sms error',toPhoneNumbers,params,err);
        failedCallback&&failedCallback(err);
    });
}

//发送短信 制定模版代码
SMS.prototype.SendTemplate=function(toPhoneNumbers,params,templateCode,successCallback,failedCallback){
    this.client.sendSMS({
        PhoneNumbers:toPhoneNumbers,
        SignName:this.SignName,
        TemplateCode:templateCode,
        TemplateParam:params
    }).then(function(res){
        console.log('send sms',res);
        if(res.Message=='OK'){
            console.log('send sms success',toPhoneNumbers,params,res);
            successCallback&&successCallback();
        }
    },function(err){
        console.log('send sms error',toPhoneNumbers,params,err);
        failedCallback&&failedCallback(err);
    });
}